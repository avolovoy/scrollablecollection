//
//  FirstColumnCollectionViewCell.swift
//  TestLayout
//
//  Created by Alex Volovoy on 4/11/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class FirstColumnCollectionViewCell: UICollectionViewCell, ReusableLabelView {
    @IBOutlet weak var contentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
           }
    
    override func layoutSubviews() {
        super.layoutSubviews()

    }

}
