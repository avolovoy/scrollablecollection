//
//  CollectionViewCell.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/14/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit


class CollectionViewCell: UICollectionViewCell, SynchronizableShadowableView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var leftColumnLayout: StickyLeftColumnViewLayout!
    var leftViewConstraint: NSLayoutConstraint!
    var rightShadowView: ShadowGradientView!
    var leftShadowView: ShadowGradientView!
    var dataSource: CollectionCellDataSource!


    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.registerCellNib(ContentCollectionViewCell.self)
        collectionView.registerCellNib(FirstColumnCollectionViewCell.self)
        setupShadows()
    }
    
     func updateCollectionView(model: ScrollableDataModel, delegate: UICollectionViewDelegate , viewBoundsWidth: CGFloat ) {
        dataSource = CollectionCellDataSource(firstCellIdentifier: FirstColumnCollectionViewCell.reuseIdentifier, сontentCellIdentifier: ContentCollectionViewCell.reuseIdentifier, model: model.model)
        collectionView.delegate = delegate
        collectionView.dataSource = dataSource
        leftColumnLayout.viewBoundsWidth = viewBoundsWidth
        leftColumnLayout.data = model
        leftViewConstraint.constant = leftColumnLayout.firstItemWidth
        collectionView.reloadData()
        
    }
}

