//
//  UIViewExtensions.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/18/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    public func addStrokes (addToTop showTop: Bool, addToBottom showBottom : Bool) {
        let constant = 1.0 / UIScreen.main.scale
        let metrics = ["line":constant, "margin": self.bounds.height - constant]
        let cbsBorder = UIColor(red: 230.0/255.0, green: 231.0/255.0, blue: 233.0/255.0, alpha: 1.0)
        if showTop {
            let topStrokeView = UIView(frame: CGRect(x:0, y:0, width:frame.width, height:constant))
            topStrokeView.translatesAutoresizingMaskIntoConstraints = false
            topStrokeView.backgroundColor = cbsBorder
            addSubview(topStrokeView)
            let views = ["topStrokeView":topStrokeView]
            let hTopConstraints = NSLayoutConstraint.constraints(withVisualFormat: "|[topStrokeView]|", options: [], metrics: metrics, views: views)
            let vTopConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[topStrokeView(line)]", options: [], metrics: metrics, views: views)
            self.addConstraints(hTopConstraints + vTopConstraints)
            
        }
        if showBottom {
            let bottomStrokeView = UIView(frame: CGRect(x:0, y:0, width:frame.width, height:constant))
            bottomStrokeView.translatesAutoresizingMaskIntoConstraints = false
            bottomStrokeView.backgroundColor = cbsBorder
            addSubview(bottomStrokeView)
            let views = ["bottomStrokeView":bottomStrokeView]
            let hBottomConstraints = NSLayoutConstraint.constraints(withVisualFormat: "|[bottomStrokeView]|", options: [], metrics: metrics, views: views)
            let vBottomConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-margin-[bottomStrokeView(line)]|", options: [], metrics: metrics, views: views)
            self.addConstraints(hBottomConstraints + vBottomConstraints)
            
        }
    }

}
