//
//  SectionCollectionViewHeader.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/19/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class SectionCollectionViewHeader: UITableViewHeaderFooterView, SynchronizableShadowableView {
    
    @IBOutlet weak var leftColumnLayout: StickyLeftColumnViewLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource: CollectionCellDataSource!
    var leftViewConstraint: NSLayoutConstraint!
    var rightShadowView: ShadowGradientView!
    var leftShadowView: ShadowGradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.registerCellNib(HeaderCollectionViewCell.self)
        collectionView.registerCellNib(HeaderFirstColumnCollectionViewCell.self)
        setupShadows()
    }
    
   func updateCollectionView(model: ScrollableDataModel, delegate: UICollectionViewDelegate , viewBoundsWidth: CGFloat ){
        dataSource = CollectionCellDataSource(firstCellIdentifier: HeaderFirstColumnCollectionViewCell.reuseIdentifier, сontentCellIdentifier: HeaderCollectionViewCell.reuseIdentifier, model: model.model)
        collectionView.delegate = delegate
        collectionView.dataSource = dataSource
        leftColumnLayout.viewBoundsWidth = viewBoundsWidth
        leftColumnLayout.data = model
        leftViewConstraint.constant = leftColumnLayout.firstItemWidth
        collectionView.reloadData()
        
    }
}


