//
//  ViewController.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/13/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

protocol StickyCollectionViewProtocol {
    var scrollableSections: [Int] { get }
}

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    var currentXOffset: CGFloat = 0

    var scrollableDataModel: ScrollableDataModel!
    var scrollableHeaderModel: ScrollableDataModel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Custom Collection View Layout"
        let rowHeight: CGFloat = 28
        let columnWidths: [ColumSize] = [.title, .medium, .large, .large, .medium, .medium, .medium]
       
        scrollableDataModel = ScrollableDataModel.make(dataModel: mockData(columns: 7, rows: 11), columnWidths: columnWidths, rowHeight: rowHeight)
        scrollableHeaderModel = ScrollableDataModel.make(dataModel: [["Title", "Col1", "Col2", "Col3", "Col4", "Col5", "Col6"]], columnWidths: columnWidths, rowHeight: rowHeight)
        
        self.collectionView.register(UINib(nibName: "OuterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: String(describing: OuterCollectionViewCell.self))
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: String(describing: CollectionViewCell.self))
        self.collectionView.register(UINib(nibName: "CollectionHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String(describing: CollectionHeaderView.self))
        self.collectionView.register(UINib(nibName: "ScrollableReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String(describing: ScrollableReusableView.self))
        flowLayout.sectionHeadersPinToVisibleBounds = true
        

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            self.collectionView.reloadData()
        })
    }
    
    // MARK UICollectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width: CGFloat = min(600, collectionView.bounds.size.width)
        return CGSize(width: width, height: 28)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = min(600, collectionView.bounds.size.width)
        if scrollableSections.contains(indexPath.section) {
            return CGSize(width: width, height: scrollableDataModel.contentHeight)
        }
        return CGSize(width: width, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var sideInset: CGFloat = 0
        if collectionView.bounds.size.width > 600 {
            sideInset = (collectionView.bounds.size.width-600.0) / 2.0
        }
        return UIEdgeInsetsMake(0, sideInset, 0, sideInset)
    }
    // MARK - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if scrollableSections.contains(section) {
            return 1
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if scrollableSections.contains(indexPath.section) {
           
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier:  String(describing: ScrollableReusableView.self), for: indexPath) as! ScrollableReusableView
            header.updateCollectionView(model: scrollableHeaderModel, delegate: self, viewBoundsWidth: collectionView.bounds.width)
            header.collectionView.tag = indexPath.section
            return header
        }
         return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String(describing: CollectionHeaderView.self), for: indexPath)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if scrollableSections.contains(indexPath.section), let header = view as? ScrollableReusableView {
            header.didScroll(offset: currentXOffset)

        }
    }
 
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            if scrollableSections.contains(indexPath.section) {
                let collectionViewCell : CollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: String(describing: CollectionViewCell.self), for: indexPath) as! CollectionViewCell
                
                collectionViewCell.updateCollectionView(model: scrollableDataModel, delegate: self, viewBoundsWidth: collectionView.bounds.width)
                collectionViewCell.collectionView.tag = indexPath.section
                collectionViewCell.didScroll(offset: currentXOffset)
                return collectionViewCell
            } else {
                let contentCell : OuterCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: String(describing: OuterCollectionViewCell.self), for: indexPath) as! OuterCollectionViewCell
                    contentCell.addStrokes(addToTop: false, addToBottom: true)

                return contentCell
            }
        }

}

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollableSections.contains(scrollView.tag), let header = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: scrollView.tag)) as? ScrollableReusableView,
            let cell = collectionView.cellForItem(at: IndexPath(item: 0, section: scrollView.tag)) as? CollectionViewCell else {return}
        if header.collectionView == scrollView {
            currentXOffset = header.collectionView.contentOffset.x
            cell.didScroll(offset: currentXOffset)
        } else {
            currentXOffset = cell.collectionView.contentOffset.x
            header.didScroll(offset: currentXOffset)
        }
    }
}

extension ViewController: StickyCollectionViewProtocol {
    var scrollableSections: [Int] {
        return [2]
    }
}

