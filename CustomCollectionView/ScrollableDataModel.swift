//
//  ScrollableDataSource.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/27/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit


enum ColumSize : CGFloat {
    case small = 26
    case medium = 38
    case large = 50
    case title = 110
}

func mockData(columns: Int, rows: Int) -> [[String]] {
    let columns = [Int](0...columns-1)
    let rows = [Int](0...rows-1)
    var model = [[String]]()
    for row in rows {
        var rowArray = [String]()
        for column in columns {
            rowArray.append("\(row):\(column)")
        }
        model.append(rowArray)
    }
    return model
}

class ScrollableDataModel: NSObject {
    var columnWidths: [ColumSize]?
    var rowHeight :CGFloat = 0
    var contentHeight :CGFloat = 0
    var model:[[String]]!
    let margin: CGFloat = 5
    
    class func make(dataModel:[[String]], columnWidths: [ColumSize]?, rowHeight: CGFloat) -> ScrollableDataModel {
        let scrollableDataModel = ScrollableDataModel()
        scrollableDataModel.model = dataModel
        scrollableDataModel.columnWidths = columnWidths
        scrollableDataModel.rowHeight = rowHeight
        scrollableDataModel.contentHeight =  rowHeight * CGFloat(dataModel.count)

        return scrollableDataModel
    } 
}
