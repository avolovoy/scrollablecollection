//
//  StickyLeftColumnViewLayout.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/17/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class StickyLeftColumnViewLayout: UICollectionViewLayout {
  
    var itemAttributes: [[UICollectionViewLayoutAttributes]]?
    var viewBoundsWidth: CGFloat = 0
    var contentSize: CGSize = .zero
    var itemsSizes: [CGSize]!
    var firstItemWidth: CGFloat = 0

    var data: ScrollableDataModel! {
        didSet {
          firstItemWidth = 0
          itemAttributes = nil
          itemsSizes = nil
          contentSize = .zero
          calculateItemsSize()
          prepare()
        }
    }
    
    func calculateItemsSize() {
        itemsSizes = []
        guard let columnWidths = data.columnWidths else { return  }
        let columnsWidth = columnWidths.reduce(0) { return $0 + $1.rawValue }
        contentSize.width = max(columnsWidth + data.margin, viewBoundsWidth)
        contentSize.height = data.contentHeight
        
        for index in 0...data.model[0].count - 1 {
            var itemSize = sizeForItemFor(columnIndex:index)
            if index == 0 {
                if columnsWidth < viewBoundsWidth {
                    itemSize.width = viewBoundsWidth - (columnsWidth - itemSize.width) - data.margin
                }
                firstItemWidth = itemSize.width
            }
            itemsSizes.append(itemSize)
        }
    }
    
    func sizeForItemFor(columnIndex: Int) -> CGSize {
        guard let columnWidths = data.columnWidths else { return .zero }
        return CGSize(width: columnWidths[columnIndex].rawValue, height: data.rowHeight)
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func prepare() {
        guard let collectionView = collectionView, collectionView.numberOfSections > 0  else {
            return
        }
        
        if let itemAttributes = itemAttributes, itemAttributes.count > 0 {
            return
        }
        
        var column = 0
        var xOffset : CGFloat = 0
        var yOffset : CGFloat = 0
        var contentWidth : CGFloat = 0
        
        for section in 0..<self.collectionView!.numberOfSections {
            var sectionAttributes: [UICollectionViewLayoutAttributes] = []
            let numberOfItems : Int = self.collectionView!.numberOfItems(inSection: section)
            
            for index in 0..<numberOfItems {
                let itemSize = itemsSizes[index]
                let indexPath = IndexPath(item: index, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
     
                if index == 0 {
                    attributes.zIndex = 1024
                    var frame = attributes.frame
                    frame.origin.x = self.collectionView!.contentOffset.x
                    attributes.frame = frame
                }
                
                sectionAttributes.append(attributes)
                
                xOffset += itemSize.width
                column += 1
                
                if column == numberOfItems {
                    if xOffset > contentWidth {
                        contentWidth = xOffset
                    }
                    
                    column = 0
                    xOffset = 0
                    yOffset += itemSize.height
                }
            }
            if itemAttributes == nil {
                itemAttributes = []
            }
            itemAttributes?.append(sectionAttributes)
        }
    }

    
    
    override var collectionViewContentSize : CGSize {
        return contentSize
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let section = itemAttributes?[indexPath.section], section.count > 0  else {
            return nil
        }
        let attributes = section[indexPath.row]
        
        return attributes
    }
    
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attributes = [UICollectionViewLayoutAttributes]()
        guard let itemAttributes = itemAttributes else { return attributes }
        for section in itemAttributes {
            
            let filteredArray = section.filter( { (evaluatedObject: UICollectionViewLayoutAttributes) -> Bool in
                return rect.intersects(evaluatedObject.frame)
            })
           
            attributes.append(contentsOf: filteredArray)
        }
        // Set frame for first column
        attributes.forEach { attributes in
            if attributes.representedElementCategory == .cell && attributes.indexPath.item == 0 {
                attributes.frame.origin.x = self.collectionView!.contentOffset.x
            }
            
        }
        return attributes
    }

    
}
