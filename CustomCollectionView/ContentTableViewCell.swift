//
//  ContentTableViewCell.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/19/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class ContentTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
