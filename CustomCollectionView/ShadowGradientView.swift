//
//  ShadowGradientView.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/27/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

enum ShadowGradientStyle {
    case right
    case left
}
class ShadowGradientView: UIView {
    
    var style: ShadowGradientStyle = .right {
        didSet {
            updateColor()
        }
    }
    let gradientLayer = CAGradientLayer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        layer.insertSublayer(gradientLayer, at: 0)
        updateColor()
    }
    
    private func updateColor() {
        let color: UIColor = UIColor(red: 230.0/255.0, green: 231.0/255.0, blue: 233.0/255.0, alpha: 0.8)
        gradientLayer.colors = [color.cgColor, color.withAlphaComponent(0).cgColor]

        if style == .right {
            gradientLayer.startPoint = CGPoint(x: 1, y: 0)
            gradientLayer.endPoint = .zero
        } else {
            gradientLayer.startPoint = .zero
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)

        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
}
