//
//  TableViewController.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/19/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var scrollableDataModel: ScrollableDataModel!
    var scrollableHeaderModel: ScrollableDataModel!
    var contentOffsetX: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let columnWidths: [ColumSize] = [.title, .medium, .small, .medium, .medium, .large, .medium, .medium, .medium]
        let rowHeight: CGFloat = 28
        scrollableDataModel = ScrollableDataModel.make(dataModel: mockData(columns: 9, rows: 11), columnWidths: columnWidths, rowHeight: rowHeight)
        scrollableHeaderModel = ScrollableDataModel.make( dataModel: [["Col0", "Col1", "Col2", "Col3", "Col4", "Col5", "Col6", "Col7", "Col8"]], columnWidths: columnWidths, rowHeight: rowHeight)
        
        self.tableView.register(UINib(nibName: "CollectionViewTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: CollectionViewTableViewCell.self))
        self.tableView.register(UINib(nibName: "ContentTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: ContentTableViewCell.self))
        self.tableView.register(UINib(nibName: "SectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: SectionHeaderView.self))
        self.tableView.register(UINib(nibName: "SectionCollectionViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: SectionCollectionViewHeader.self))
   
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if scrollableSections.contains(section) {
            
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SectionCollectionViewHeader.self)) as! SectionCollectionViewHeader
            header.updateCollectionView(model: scrollableHeaderModel, delegate: self, viewBoundsWidth: tableView.bounds.width)
            header.collectionView.tag = section
            return header
        }
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SectionHeaderView.self)) as! SectionHeaderView
        header.contentLabel.text = "Header for Section \(section)"
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if scrollableSections.contains(indexPath.section) {
            return scrollableDataModel.contentHeight + 1 // for separator
           
        }
        return 44
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if scrollableSections.contains(section) {
            return 1
        }
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if scrollableSections.contains(indexPath.section) {
            let cell =  tableView.dequeueReusableCell(withIdentifier: String(describing: CollectionViewTableViewCell.self)) as! CollectionViewTableViewCell
            cell.updateCollectionView(model: scrollableDataModel, delegate: self, viewBoundsWidth: tableView.bounds.width)
            cell.collectionView.tag = indexPath.section
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier:  String(describing: ContentTableViewCell.self), for: indexPath) as! ContentTableViewCell
        cell.contentLabel.text = "\(indexPath.section) : \(indexPath.item)"

        return cell
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollableSections.contains(scrollView.tag), let header = tableView.headerView(forSection: scrollView.tag)
            as? SectionCollectionViewHeader, let collectionViewCell = tableView.cellForRow(at:IndexPath(item: 0, section: scrollView.tag)) as? CollectionViewTableViewCell
            else { return }
        
        if header.collectionView == scrollView {
            contentOffsetX = header.collectionView.contentOffset.x
            collectionViewCell.didScroll(offset: contentOffsetX)

        } else {
            contentOffsetX = collectionViewCell.collectionView.contentOffset.x
            header.didScroll(offset: contentOffsetX)
        }
    }

}
extension TableViewController: UICollectionViewDelegate {
    // We need this so we can pass self as delegate for CollectionView in order get didScroll Event
}

extension TableViewController: StickyCollectionViewProtocol {
    var scrollableSections: [Int] {
        return [0,2]
    }
}
