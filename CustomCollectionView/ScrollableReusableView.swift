//
//  ScrollableReusableView.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/18/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class ScrollableReusableView: UICollectionReusableView, SynchronizableShadowableView {

    @IBOutlet weak var leftColumnLayout: StickyLeftColumnViewLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    var leftViewConstraint: NSLayoutConstraint!
    var rightShadowView: ShadowGradientView!
    var leftShadowView: ShadowGradientView!
    var dataSource: CollectionCellDataSource!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.registerCellNib(HeaderCollectionViewCell.self)
        collectionView.registerCellNib(HeaderFirstColumnCollectionViewCell.self)
        setupShadows()
    }
    
     func updateCollectionView(model: ScrollableDataModel, delegate: UICollectionViewDelegate, viewBoundsWidth: CGFloat ) {
        dataSource = CollectionCellDataSource(firstCellIdentifier: HeaderFirstColumnCollectionViewCell.reuseIdentifier, сontentCellIdentifier: HeaderCollectionViewCell.reuseIdentifier, model: model.model)
        collectionView.delegate = delegate
        collectionView.dataSource = dataSource
        leftColumnLayout.viewBoundsWidth = viewBoundsWidth
        leftColumnLayout.data = model
        leftViewConstraint.constant = leftColumnLayout.firstItemWidth
        collectionView.reloadData()

    }
}
