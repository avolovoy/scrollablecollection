//
//  CollectionCellDataSource.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/28/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

protocol ReusableLabelView: class {
    static var reuseIdentifier: String { get }
    var contentLabel: UILabel! { get set }
}

extension ReusableLabelView where Self: UICollectionViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionView {
    
    func registerCellNib<T: UICollectionViewCell>(_: T.Type) where T: ReusableLabelView {
        let nib = UINib(nibName: NSStringFromClass(T.self).components(separatedBy: ".").last!, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerCellClass<T: UICollectionViewCell>(_: T.Type) where T: ReusableLabelView {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
}

class CollectionCellDataSource: NSObject, UICollectionViewDataSource  {
    var model:[[String]]!
    var firstCellIdentifier:String!
    var сontentCellIdentifier:String!
   
    convenience init(firstCellIdentifier: String, сontentCellIdentifier: String , model:[[String]]) {
        self.init()
        self.firstCellIdentifier = firstCellIdentifier
        self.сontentCellIdentifier = сontentCellIdentifier
        self.model = model
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifer:String = indexPath.item == 0 ? firstCellIdentifier : сontentCellIdentifier
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifer, for: indexPath)
        if let reusableCell = cell as? ReusableLabelView {
            reusableCell.contentLabel.text = model[indexPath.section][indexPath.item]
            
        }
        cell.addStrokes(addToTop: false, addToBottom: true)
        
        return cell
    }
}
