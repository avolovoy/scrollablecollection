//
//  HeaderCollectionViewCell.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/18/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell, ReusableLabelView {

    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

}
