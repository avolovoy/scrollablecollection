//
//  SectionHeaderView.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/19/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class SectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

}
