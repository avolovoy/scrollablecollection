//
//  SectionsTableViewController.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/24/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import UIKit

class SectionsTableViewController: UITableViewController {

    
    var scrollableDataModel: ScrollableDataModel!
    var scrollableHeaderModel: ScrollableDataModel!
    

    var contentOffsetX :CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
         let columnWidths: [ColumSize] = [.title, .medium, .small, .medium, .medium, .large, .medium, .medium, .medium]
         let rowHeight: CGFloat = 28
        
         scrollableDataModel = ScrollableDataModel.make(dataModel: mockData(columns: 9, rows: 11), columnWidths: columnWidths, rowHeight: rowHeight)
         scrollableHeaderModel = ScrollableDataModel.make( dataModel: [["Col0", "Col1", "Col2", "Col3", "Col4", "Col5", "Col6", "Col7", "Col8"]], columnWidths: columnWidths, rowHeight: rowHeight)
        
        self.tableView.register(UINib(nibName: "CollectionViewTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: CollectionViewTableViewCell.self))
        self.tableView.register(UINib(nibName: "ContentTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: ContentTableViewCell.self))
        self.tableView.register(UINib(nibName: "SectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: SectionHeaderView.self))
        self.tableView.register(UINib(nibName: "SectionCollectionViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: SectionCollectionViewHeader.self))
        tableView.separatorStyle = .none
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SectionCollectionViewHeader.self)) as! SectionCollectionViewHeader
        header.updateCollectionView(model: scrollableHeaderModel, delegate: self, viewBoundsWidth: tableView.bounds.width)
        header.collectionView.tag = section
        header.didScroll(offset: contentOffsetX)

        return header
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return scrollableDataModel.contentHeight
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
          }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell =  tableView.dequeueReusableCell(withIdentifier: String(describing: CollectionViewTableViewCell.self)) as! CollectionViewTableViewCell
            cell.updateCollectionView(model: scrollableDataModel, delegate: self, viewBoundsWidth: tableView.bounds.width)
            cell.collectionView.tag = indexPath.section
            cell.didScroll(offset: contentOffsetX)

            return cell
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let header = tableView.headerView(forSection: scrollView.tag)
            as? SectionCollectionViewHeader, let collectionViewCell = tableView.cellForRow(at:IndexPath(item: 0, section: scrollView.tag)) as? CollectionViewTableViewCell
            else { return }
        
        if header.collectionView == scrollView {
            contentOffsetX = header.collectionView.contentOffset.x
            collectionViewCell.collectionView.contentOffset.x = header.collectionView.contentOffset.x
        } else {
            contentOffsetX = collectionViewCell.collectionView.contentOffset.x
        }
        guard let visibleCells = tableView.visibleCells as? [CollectionViewTableViewCell] else { return }
        for cell in visibleCells  {
            cell.didScroll(offset: contentOffsetX)
        }
        
        guard let visibleRows = tableView.indexPathsForVisibleRows else {
            return
        }
        let visibleSections = visibleRows.map({$0.section})
        for section in visibleSections {
            if let header = tableView.headerView(forSection: section) as? SectionCollectionViewHeader {
                header.didScroll(offset: contentOffsetX)
   
            }
        }
        
    }
    
}


extension SectionsTableViewController: UICollectionViewDelegate {
    // We need this so we can pass self as delegate for CollectionView in order get didScroll Event
}
