//
//  ShadowableView.swift
//  CustomCollectionView
//
//  Created by Alex Volovoy on 4/28/17.
//  Copyright © 2017 Alex Volovoy. All rights reserved.
//

import Foundation
import UIKit

protocol SynchronizableShadowableView: class {
    var leftViewConstraint: NSLayoutConstraint! {get set}
    var rightShadowView: ShadowGradientView!  {get set}
    var leftShadowView: ShadowGradientView!  {get set}
    var collectionView: UICollectionView!  {get set}
    var leftColumnLayout: StickyLeftColumnViewLayout! {get set}
    var dataSource: CollectionCellDataSource! {get set}

    func setupShadows()
    func didScroll(offset: CGFloat)
    func updateCollectionView(model: ScrollableDataModel, delegate: UICollectionViewDelegate , viewBoundsWidth: CGFloat )

}

extension SynchronizableShadowableView where Self: UIView {
    func setupShadows() {
        rightShadowView = ShadowGradientView ()
        rightShadowView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(rightShadowView)
        
        rightShadowView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        rightShadowView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        rightShadowView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        rightShadowView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        
        
        leftShadowView = ShadowGradientView ()
        leftShadowView.style = .left
        leftShadowView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(leftShadowView)
        
        leftShadowView.widthAnchor.constraint(equalToConstant: 5).isActive = true
        leftShadowView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        leftShadowView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        leftViewConstraint = leftShadowView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 110)
        leftViewConstraint.isActive = true

    }
    
    func didScroll(offset: CGFloat) {
        collectionView.contentOffset.x = offset
        leftShadowView.isHidden = offset <= 0
        rightShadowView.isHidden = leftColumnLayout.collectionViewContentSize.width - offset  <= collectionView.bounds.width
    }

}
